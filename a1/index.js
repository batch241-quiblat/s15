const firstName = "Shane Mar";
const lastName = "Quiblat";
let age = 23;
const hobbies = ["Playing Physical Sports","Playing E-Sports","Watching Anime"];
const workAddress = {
	houseNumber: "B9-H6",
	street: "Gwapo St.",
	city: "Cagayan De Oro City",
	state: "Mis. Or."
};


const fullName = "Steve Rogers";
let currentAge = 40;
const friends = ["Tony","Thor","Natasha","Clint","Nick"];
const profile = {

};
const fullProfile = {
	username: "captain_america",
	fullName: "Steve Rogers",
	age: 40,
	isActive: false
};
const bestfriend = "Bucky Barnes";
const foundInLocation = "Artic Ocean"; 

console.log("First Name: " + firstName);
console.log("Last Name: " + lastName);
console.log("Age: " + age);
console.log("Hobbies:");
console.log(hobbies);
console.log("Work Address:");
console.log(workAddress);


console.log("My full name is: " + fullName);
console.log("My current age is: " + currentAge);
console.log("My friends are:");
console.log(friends);
console.log("My Full Profile:");
console.log(fullProfile);
console.log("My bestfriend is: " + bestfriend);
console.log("I was found frozen in: " + foundInLocation);
