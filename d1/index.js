console.log("Hello World!");

// Syntax of console.log; console.log(variable to String)

// It will comment parts of the code that gets ignored by the language

/*
	There are two type of components
	1. The single line comment denoted by two forward slashes.
	2. The multi-line comeent denoted by slash and asterisk
*/

// [Section] Syntax, statements
	// Statements in programming are instructions that we tell the computer to perform
	// JS statements usually end with semicolon (;)
	// Semicolon are not required in JS but we will use it to help us train to locate where statement ends.
	// A syntax in programming, it is the set of rules that we describes statements must be constructed
	// All lines/blocks of code should be written in specific manner to work. This is due to how these codes where initially programmed to function in a certain manner.

// [Section] Variable
	// Variables are used to contain data
	// Any information that is used by an application is stored in what we call the memory
	// When we create variables, certain portions of device's memory is given a name that we call variables

	// This makes is easier for us to associate information stored in our devices to actual "names" information

	// Declaring Variables - it tells our devices that a variable name is created and is ready to store data.
		// Syntax
			/*let/const variableName;*/

	let myVariable = "Ada Lovelace";

	let variable;
	// This will cause an undefined variable because the declared variable does not have initail value.
	console.log(variable);

	// const keyword is used when the value of the variable won't change.
	const constVariable = "John Doe";

	// console.log() is useful for printing values of variables or certain of code into the browser's console.
	console.log(myVariable); 

	/*
		Guides in writing Variables
			1. Use the let keyword followed the variable name of your choose and use the addignment operator (=) to assign value.
			2. Variable names should start with lowercase character, use camelCasing for the multiple words.
			3. For constant variables, use the 'const keyword'
				Note: If we use const keyword in declaring a variable, we cannot change the value of its variable.
			4. Variable names should be indicative(descriptive) of the value being stored to avoid confusion.
	*/

	// Declare and initialize
		// Initializing variables - the instance when a variable is givin its first/initial value
		// Syntax:
			// let/const variableName = initial value;

	// Declaration and Initialization of the variable occur

	let productName = "desktop computer";
	console.log(productName);

	// Declaration
	let desktopName;
	console.log(desktopName);

	// Initialization of the value of variable desktopName
	desktopName = "Dell";
	console.log(desktopName);

	// Reassigning value
			// Syntax: variableName = newValue
	productName = "Personal Computer"
	console.log(productName);

	const name = "Shane";
	console.log(name);

	// This reassignment will cause an error since we cannot change/reassign the initial value of a constant variable.
	/*name = "Mar";
	console.log(name);*/

	//This will cause an error on our code because the productName variable is already taken.
	/*let productName = "Laptop";*/

	// var vs le/const
		// some of you may wonder why we used let and const keyword in declaring a vriable when we search online, we usually see var.

		// var - is alse used in declaring variable but var is an ecmaScript1 feature [(1997)].

	let lName;
	lName = "Vic";
	console.log(lName);

/*
	Using var, bad practice.
	batch = "Batch 241";

	var batch;
	console.log(batch);
*/


//let/const local/global scope
/*
	Scope essentially means where these variables arre available for use

	let/const are block scope

	A block is a chuck of code bounded by a {}.
*/


/*let outerVariable = "hello";
{
	let innerVariable = "Hello Again";
	console.log(innerVariable);
}

console.log(outerVariable);*/


/*const outerVariable = "Hello";
{
	const innerVariable = "Hello Again";
}

console.log(outerVariable);
console.log(innerVariable);*/

var outerVariable = "Hello";
{
	var innerVariable = "Hello Again";
}

console.log(outerVariable);
console.log(innerVariable);

//Declaring Multiple Variable

/*let productCode = "DC017" , productBrand = "Dell";

onsole.log(productCode);
console.log(productBrand);*/

/*//Using a variable with a reserved keyword
const let = "Hello";
console.log(let);*/

// [SECTION] Data Types
//Strings
/*
	Strings are a series of characters that create a word, a phrase, a sentence or anything related to creating a text
	Strings in JavaScript can be written using either a single ('') or double ("") qoutes.
*/
let country = "Philipinnes";
let province = 'Metro Manila';
console.log(country);
console.log(province);

// Concatenate strings
// Multiple strings values can be combined to create a single string using "+" symbol.

let fullAddress = province + ' ' + country;
console.log(fullAddress);

let greeting = 'I live in the ' + country;
console.log(greeting);

// Declaring a string using an escape character
let message = 'John\'s employees went home early';
console.log(message);

message = "John's employees went home early";
console.log(message);

// "\n" - refers to creating a new line in between text
let mailAddress = "Metro Manila\n\nPhilippines";
console.log(mailAddress);
console.log(typeof mailAddress);

// Numbers
//Integer/Whole Number
let headCount = 27;
console.log(headCount);
console.log(typeof headCount);

//Decimal
let grade = 98.7; 
console.log(grade);
console.log(typeof grade);

//Float
let planetDistance = 2e10;
console.log(planetDistance);
console.log(typeof planetDistance);

//Combine text and strings
console.log("John's grade last quarter is " + grade);

// Boolean
// Boolean values are normally used to store values relating to the state of certain things
// true or false
let isMarried = false;
let isGoodConduct = true;
console.log(typeof isMarried);
console.log(typeof(typeof isGoodConduct));

// Arrays
// Arrays are a special kind of data type that's use to store multiple values

/*
	Syntax:
		let/const arrayName = [elementA, elementB ...];
*/
// Contains similar data types
let grades = [98, 92.1, 94.7];
console.log(grades);
// Array is a special kind of object
console.log(typeof grades);

// Contains different data types
// Storing different data types inside an array is not recommended
let details = ["john", 32, true,["hi",1]];
console.log(details);
console.log(typeof details);

// Object
// Objects are another special kind of data type that's used to mimic real world objects/ items

/*
	Syntax:
	let/const objectName = {
		propertyA: value,
		propertyB: value
	}
*/

let person = {
	firstName: "John",
	lastName: "Smith",
	age: 32,
	isMarried: false,
	contact: ["+369123456789","9123-4567"],
	address: {
		houseNumber: "345",
		city: "Manila"
	}
};
console.log(person);
console.log(typeof person);

let person1 = {};

//[98,92.1,94.7]
let myGrades = {
	firstGrading: 98,
	secondGrading: 92.1,
	thridGrading: 90.1,
	fourthGrading: 94.7
}

/*
	Constant Objects and Arrays

	We can change the element of an array to a constant variable

*/

const anime = ["one piece" , "one punch man", "your lie in April"];
console.log(anime);

// arrayName[indexNumber]
anime[0] = ["demon slayer"];
console.log(anime);

/*const anime1 = ["one piece" , "one punch man", "your lie in April"];
console.log(anime1);
anime1 = ["demon slayer"];
console.log(anime1);*/

//Null
// It is used to intentionally express the absence og a value in a variable declaration/ initialization
let spouse = null;
console.log(spouse);

let myNumber = 0;
let myString = "";

// Undefined
// Represent the state a variable that has been declared but without an assigned value.
let inARelationship;
console.log(inARelationship);